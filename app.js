$("#enregistrer").on("click", function() {
  var annuaire = localStorage.getItem("annuaire");

  if (annuaire == null) {
    annuaire = "[]";
  }
  annuaire = JSON.parse(annuaire);

  var fiche = {
    nom: $("#nom").val(),
    prenom: $("#prenom").val(),
    sexe: $("#genre").val(),
    userID: $("#user_id").val(),
    password: $("#mdp").val(),
    date_naissance: $("#date").val(),
    ville: $("#ville").val(),
    email: $("#email").val(),
    url: $("#url").val(),
    hobbies: $("#hobbies").val(),
    telephone: $("#tel").val(),
    couleur: $("#couleur").val()
  };
  
  annuaire.push(fiche);
  localStorage.setItem("annuaire", JSON.stringify(fiche));
});
